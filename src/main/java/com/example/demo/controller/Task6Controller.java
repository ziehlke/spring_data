package com.example.demo.controller;

import com.example.demo.model.Animal;
import com.example.demo.service.AnimalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class Task6Controller {
    //    private final AnimalRepository animalRepository;
    private final AnimalService animalService;

    //    Mieć metodę POST, która będzie tworzyć nowego zwierzaka i zapisywać go do bazy przy pomocy AnimalRepository
    @PostMapping("/animals")
    void newAnimal(@RequestBody Animal newAnimal) {
        animalService.save(newAnimal);
    }


    //    Mieć metodę GET, która będzie zwracać wszystkie zwierzaki z repozytorium
    @GetMapping("/animals")
    List<Animal> all() {
        return animalService.findAll();
    }

//    Mieć metodę GET, która będzie zwracać wszystkie zwierzaki starsze niż X lat oraz zawierające Y w imieniu (@RequestParam)
//    @GetMapping("/animals/{olderThan}")
//    AnimalEntity olderThanXnamedWithY(@RequestBody AnimalEntity animal) {
//
//        return animalRepository.findByname(id)
//                .orElseThrow(() -> new EmployeeNotFoundException(id));
//    }


    @GetMapping("/animals/withparam")
    List<Animal> olderThan50andNameReksio(@RequestParam int age, @RequestParam String name) {
        return animalService.findAllByAgeGreaterThanAndNameContains(age, name);
    }


    @DeleteMapping("/animals/{id}")
    ResponseEntity deleteAnimal(@PathVariable UUID id) {
        if (animalService.deleteById(id).isPresent()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping("animals/{id}")
    ResponseEntity<Object> findById(@PathVariable UUID id) {
        return animalService.findById(id).isPresent() ?
                ResponseEntity.status(HttpStatus.ACCEPTED).body(animalService.findById(id).get()) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @PostMapping("animals/add-many")
    List<Animal> newAnimals(@RequestBody List<Animal> animals) {
        return animalService.saveAll(animals);
    }


    @GetMapping("/animals/paged")
    List<Animal> pagedRecords(@RequestParam int recordsPerPage, @RequestParam int pageNumber){
        return animalService.pagedRecords(recordsPerPage, pageNumber);
    }


/*    @PutMapping("/employees/{id}")
    Employee replaceEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {

        return repository.findById(id)
                .map(employee -> {
                    employee.setName(newEmployee.getName());
                    employee.setRole(newEmployee.getRole());
                    return repository.save(employee);
                })
                .orElseGet(() -> {
                    newEmployee.setId(id);
                    return repository.save(newEmployee);
                });
    }


    @DeleteMapping("/employees/{id}")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }*/
}

