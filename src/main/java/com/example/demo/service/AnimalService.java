package com.example.demo.service;

import com.example.demo.domain.model.AnimalEntity;
import com.example.demo.domain.repository.AnimalRepository;
import com.example.demo.model.Animal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AnimalService {
    public final AnimalRepository animalRepository;

    public AnimalEntity map(Animal animal) {
        return new AnimalEntity(animal.getName(), animal.getAge());
    }

    public Animal map(AnimalEntity animalEntity) {
        return new Animal(animalEntity.getName(), animalEntity.getAge());
    }

    public void save(Animal newAnimal) {
        animalRepository.save(map(newAnimal));
    }

    public List<Animal> findAll() {
        return animalRepository.findAll().stream().map(this::map).collect(Collectors.toList());
    }


    public List<Animal> findAllByAgeGreaterThanAndNameContains(int age, String name) {
        return animalRepository.findAllByAgeGreaterThanAndNameContains(age, name).stream().map(this::map).collect(Collectors.toList());
    }

    public Optional<Animal> deleteById(UUID id) {
        if (animalRepository.existsById(id)) {
            Optional<Animal> toReturn = Optional.of(map(animalRepository.findById(id).get()));
            animalRepository.deleteById(id);
            return toReturn;
        }
        return Optional.empty();
    }

    public Optional<Animal> findById(UUID id) {
        return Optional.of(map(animalRepository.findById(id).get()));
    }

    public List<Animal> saveAll(List<Animal> animals) {
        List<AnimalEntity> animalEntities = animals.stream().map(this::map).collect(Collectors.toList());
        animalRepository.saveAll(animalEntities);
        return animals;
    }

    //    Dodajcie nową metodę w kontrolerze, która będzie pozwalała na otrzymanie listy zwierzaków oraz będzie przyjmować parametry:
//    ilośc rekordów per strona, numer oczekiwanej strony, kolejność sortowania po imieniu (@RequestParam)
    public List<Animal> pagedRecords(int recordsPerPage, int pageNumber) {
        return animalRepository.findAll(PageRequest.of(pageNumber, recordsPerPage, Sort.by("name"))).stream().map(this::map).collect(Collectors.toList());
    }

}
