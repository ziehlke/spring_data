package com.example.demo.domain.repository;

import com.example.demo.domain.model.AnimalEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AnimalRepository extends JpaRepository<AnimalEntity, UUID> {

    List<AnimalEntity> findAllByAgeGreaterThanAndNameContains(int age, String name);

    List<AnimalEntity> findAllByNameContains(String name);

    List<AnimalEntity> findAllByAgeEquals(int age);

    List<AnimalEntity> findAllByNameContainsAndAgeEquals(String name, int age);

    //    Bez kryteriów na pola, ale z limitem 3, sortowanie po name od Z do A
    List<AnimalEntity> findFirs3ByOrderByNameDesc();






}
